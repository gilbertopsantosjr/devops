
docker stats
docker network create --driver bridge mudar-network
apt-get update && apt-get install -y iputils-ping

docker run -d --name configserver --network=mudar-network --hostname=configserver -e PROFILE=native se.mudar.infra.configserver:0.0.3

docker run -it --network=mudar-network -e PROFILE=dev -e CONFIG:http://configserver:8888 se.mudar.infra.auth:0.0.7


docker exec -it stack_dns_1 hostname
docker-compose -p kafka -f docker-infra-dev.yml up

To delete all containers including its volumes use,
docker rm -vf '$(docker ps -a -q)'
docker rm $(docker ps -aq)

To delete all network
docker network rm $(docker network ls -q)

To delete all the images,
docker rmi -f $(docker images -a -q)

docker pull gcr.io/mudar-se/infra/auth:latest


//https://blog.container-solutions.com/using-google-container-registry-with-kubernetes
1) kubectl create namespace localdev

kubectl config set-context docker-desktop --namespace=localdev

2) kubectl config set-context --namespace=localdev docker-for-desktop
	to change difference environament see: 
	 kubectl config get-contexts ( all environments )
	 kubectl config current-context
	 kubectl config use-context ${env_name}
 
3)
// If you're on windows use ' rather than "
// a new service account must hold a 'Viewer all resources' Role into GCP
kubectl create secret docker-registry gcr-json-key --docker-server=gcr.io --docker-username=_json_key --docker-email=gilbertopsantosjr@gmail.com --docker-password="$(cat mudar-se-2e2f49085f11.json)"

// If it already exist you may run the below command:
kubectl delete secret gcr-json-key

4) add the new key into your environment
kubectl patch serviceaccount default -p '{"imagePullSecrets": [{"name": "gcr-json-key"}]}'

5) check if the imagePullSecrets is the new one just created.
kubectl get serviceaccount default -o yaml


6) 
//to see the logs of a pod type 
kubectl describe pod $name


Execute the pod in interactive mode 
kubectl exec -it ${pod_name} --bash


7) how to get an external IP for a node 
7.1 first create a new server with a type NodePort
7.2 label your pod to this service 
then execute. 
kubectl get nodes -o wide 


